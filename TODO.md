# Dev Candidate Challenge

Thank you for taking the time to participate in the Dev Candidate Challenge. This challenge will assist us in better understanding your react knowledge and, more importantly, your problem-solving process.

Please limit your time to no more than 2 hours for this challenge. Don't be concerned if you are unable to complete a fully functional solution within this time frame. We are primarily interested in understanding your thought process as you work through these types of problems. Before you code, feel free to use comments to scaffold your ideas.

The instructions are purposefully vague. Please use your best judgment, document your questions as you go, and contact us if you encounter any problems that prevent you from completing the challenge.

## Task 1: Reusable Components

In `src/App.js`, you will find an app shell component. It is not a complete app, but a starting point that contains what might be a sidebar component with a series of sorting and filter options. 

This app was not structure for reusability. It returns a large block of html with redundant inline styles. How would you refactor this for reusable components that do not use inline styles? Feel free to reorganize the directory structure as you see fit.

## Task 2: State Management

For this app, the url is treated as the source of truth for state. Sorting & filtering options can be specified in the querystring of the url.

For example, if the url contains `?sort=partner-rating`, the `Partner rating` option in the `sort` box should be selected. When the user selects a different option, say `Most reviews`, the url should update to reflect the new selection.

Write a custom react hook that will keep track of the query params and update the state of the sort, delivery method, time zone, country, and partner rating selections when the url changes. Use the following query params for tests:

* ?sort=partner-rating&delivery-method=virtual&timezone=mst&country=us&partner-rating=4
* ?sort=most-reviews&delivery-method=in-person&timezone=cst&country=mexico&partner-rating=3