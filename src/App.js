import 'normalize.css';

function App() {
  return (
    <div style={{
      padding: '1.5rem'
    }}>
      <div style={{
        display: 'grid',
        gridGap: '1.5rem',
      }}>
        <div style={{
          border: '1px solid #dfdfdf',
          borderRadius: '0.25rem',
          padding: '1rem',
          display: 'grid',
          gridGap: '1rem',
        }}>
          <h3 style={{
            margin: 0
          }}>Sort</h3>
          <ul style={{
            margin: 0,
            padding: 0,
            listStyle: 'none',
            display: 'grid',
            gridGap: '0.25rem'
          }}>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="partner-level" />
              <label>Partner level</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="partner-rating" />
              <label>Partner rating</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="review-count" />
              <label>Most reviews</label>
            </li>
          </ul>
        </div>

        <div style={{
          border: '1px solid #dfdfdf',
          borderRadius: '0.25rem',
          padding: '0.5rem',
          padding: '1rem',
          display: 'grid',
          gridGap: '1rem',
        }}>
          <h3 style={{
            margin: 0
          }}>Delivery method</h3>
          <ul style={{
            margin: 0,
            padding: 0,
            listStyle: 'none',
            display: 'grid',
            gridGap: '0.25rem'
          }}>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="all" />
              <label>All</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="virtual" />
              <label>Virtual</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="radio" name="sort" value="in-person" />
              <label>In-person</label>
            </li>
          </ul>
        </div>

        <div style={{
          border: '1px solid #dfdfdf',
          borderRadius: '0.25rem',
          padding: '1rem',
          display: 'grid',
          gridGap: '1rem',
        }}>
          <h3 style={{
            margin: 0
          }}>Time zone</h3>
          <select>
            <option value="">Select a time zone</option>
            <option value="est">Eastern</option>
            <option value="cst">Central</option>
            <option value="mst">Mountain</option>
            <option value="pst">Pacific</option>
          </select>
        </div>


        <div style={{
          border: '1px solid #dfdfdf',
          borderRadius: '0.25rem',
          padding: '1rem',
          display: 'grid',
          gridGap: '1rem',
        }}>
          <h3 style={{
            margin: 0
          }}>Country</h3>
          <select>
            <option value="">Select a country</option>
            <option value="US">United States</option>
            <option value="CA">Canada</option>
            <option value="MX">Mexico</option>
          </select>
        </div>

        <div style={{
          border: '1px solid #dfdfdf',
          borderRadius: '0.25rem',
          padding: '1rem',
          display: 'grid',
          gridGap: '1rem',
        }}>
          <h3 style={{
            margin: 0
          }}>Partner rating</h3>
          <ul style={{
            margin: 0,
            padding: 0,
            listStyle: 'none',
            display: 'grid',
            gridGap: '0.25rem'
          }}>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="checkbox" name="rating" value="5" />
              <label>5 stars</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="checkbox" name="rating" value="4" />
              <label>4 stars</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="checkbox" name="rating" value="3" />
              <label>3 stars</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="checkbox" name="rating" value="2" />
              <label>2 stars</label>
            </li>
            <li style={{
              display: 'grid',
              gridAutoFlow: 'column',
              gridAutoColumns: 'max-content',
              gridGap: '0.5rem',
              alignItems: 'center'
            }}>
              <input type="checkbox" name="rating" value="1" />
              <label>1 star</label>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;